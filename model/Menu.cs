﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace model
{
    public class Menu
    {
        public int menuId { get; set; }
        [Required]
        [MinLength(3), MaxLength(10)]
        public string descripcion { get; set; }
    }
}
