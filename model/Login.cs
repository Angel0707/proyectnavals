﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model
{
    public class Login
    {
        public string username { get; set; }
        public string password { get; set; }
        public int rolId { get; set; }
    }
}
