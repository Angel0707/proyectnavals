﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model
{
    public class Pagina
    {
        public int paginaId { get; set; }
        public int descripcion { get; set; }
        public int menuId { get; set; }
    }
}
