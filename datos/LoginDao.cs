﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using model;

namespace datos
{
    public class LoginDao
    {
        SqlCommand cmd = null;
        Conexion conect = null;
        SqlDataReader dr = null;

        public Login acceso(Login dato)
        {
            Login l = null;
            conect = new Conexion();
            if (conect.conn.State == ConnectionState.Closed)
            {
                conect.connectDB();
            }
            try
            {
                cmd = new SqlCommand("sp_login", conect.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@username", dato.username);
                cmd.Parameters.AddWithValue("@password", dato.password);
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                   l = new Login()
                    {
                        username = dr.GetString(0),
                        password = dr.GetString(1)
                    };
                }

            }
            catch (Exception e)
            {
                
            }
            return l;
        }

    }
}
