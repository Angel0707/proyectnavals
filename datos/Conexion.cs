﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using model;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace datos
{
    public class Conexion
    {
        public SqlConnection conn = new SqlConnection();
        public SqlConnection connectDB()
        {
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
            conn.Open();
            return conn;
        }
        public SqlConnection closeDB()
        {
            conn.Close();
            return conn;
        }
    }
}
