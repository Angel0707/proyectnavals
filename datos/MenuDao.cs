﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using model;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace datos
{
    public class MenuDao
    {
        SqlCommand cmd = null;
        Conexion conect = null;
        SqlDataReader dr = null;
        List<Menu> lst = null;

        public void addMenu(Menu dato)
        {
            conect = new Conexion();
            if (conect.conn.State == ConnectionState.Closed)
            {
                conect.connectDB();
            }

            try
            {
                cmd = new SqlCommand("sp_insert_Menu", conect.conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@desc", dato.descripcion);
                cmd.ExecuteNonQuery();
                conect.closeDB();
            }
            catch (Exception e)
            {
                conect.closeDB();
                e.Message.ToString();
            }


        }
    }
}
